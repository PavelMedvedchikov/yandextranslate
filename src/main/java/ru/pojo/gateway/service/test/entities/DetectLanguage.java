package ru.pojo.gateway.service.test.entities;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;;

@Getter
public class DetectLanguage {
    @SerializedName("languageCode")
    public String languageCode;

}
