package ru.pojo.gateway.service.test.entities;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class Language {
    @SerializedName("code")
    public String code;
    @SerializedName("name")
    public String name;
}
