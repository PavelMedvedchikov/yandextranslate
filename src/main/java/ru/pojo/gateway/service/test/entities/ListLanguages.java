package ru.pojo.gateway.service.test.entities;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;

import java.util.List;

@Getter
public class ListLanguages {
    @SerializedName("languages")
    public List<Language> languages;
}
