package ru.pojo.gateway.service.test.entities;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class Translations {
    @SerializedName("text")
    public String text;
    @SerializedName("detectedLanguageCode")
    public String detectedLanguageCode;
}
