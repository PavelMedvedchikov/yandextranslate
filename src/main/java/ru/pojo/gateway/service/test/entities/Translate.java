package ru.pojo.gateway.service.test.entities;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public class Translate {
    @SerializedName("translations")
    public List<Translations> translations;

    public ArrayList<String> getTextTranslate() {
        ArrayList<String> text = new ArrayList<>();
        for (int i=0;this.translations.size()>i; i++) {
            text.add(this.translations.get(i).getText());
        }
        return text;
    }
}
