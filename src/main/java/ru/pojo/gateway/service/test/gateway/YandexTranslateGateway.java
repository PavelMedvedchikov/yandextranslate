package ru.pojo.gateway.service.test.gateway;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.SneakyThrows;
import okhttp3.*;
import org.json.JSONObject;
import ru.pojo.gateway.service.test.entities.*;

import java.util.ArrayList;
import java.util.List;

public class YandexTranslateGateway {
    private static final String URL = "https://translate.api.cloud.yandex.net/translate/v2";
    private static final String OAuthToken = "";
    private static final String folderId = "";

    OkHttpClient client = new OkHttpClient();


    @SneakyThrows
    private String post(JSONObject json, String path) {
        RequestBody body = RequestBody.create(
                json.toString(), MediaType.get("application/json;  charset=utf-8"));
        Request request = new Request.Builder()
                .url(URL + path)
                .addHeader("Authorization", "Bearer " + OAuthToken)
                .post(body)
                .build();
        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        }
    }

    public DetectLanguage getDetectLanguage(String path, String text, List<String> languageCodeHints) {
        Gson gson = new Gson();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("text", text);
        jsonObject.put("languageCodeHints", languageCodeHints);
        jsonObject.put("folderId", folderId);
        return gson.fromJson(post(jsonObject, path), DetectLanguage.class);
    }

    public DetectLanguage getDetectLanguage(String path, String text) {
        return getDetectLanguage(path, text, new ArrayList<>());
    }

    public ListLanguages getListLanguages(String path) {
        Gson gson = new Gson();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("folderId", folderId);
        return gson.fromJson(post(jsonObject, path), ListLanguages.class);
    }

    public Translate getTranslate(ArrayList<String> texts, String path) {
        Gson gson = new GsonBuilder().create();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("targetLanguageCode", "ru");
        jsonObject.put("texts", texts);
        jsonObject.put("folderId", folderId);
        return gson.fromJson(post(jsonObject, path), Translate.class);
    }
}
