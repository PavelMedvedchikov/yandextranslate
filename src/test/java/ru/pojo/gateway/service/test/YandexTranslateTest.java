package ru.pojo.gateway.service.test;

import lombok.SneakyThrows;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.pojo.gateway.service.test.entities.DetectLanguage;
import ru.pojo.gateway.service.test.entities.ListLanguages;
import ru.pojo.gateway.service.test.entities.Translate;
import ru.pojo.gateway.service.test.gateway.YandexTranslateGateway;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class YandexTranslateTest {

    @Test
    @DisplayName("language")
    public void getLanguage() {
        YandexTranslateGateway yandexTranslateGateway = new YandexTranslateGateway();
        DetectLanguage detectLanguage = yandexTranslateGateway.getDetectLanguage("/detect","name");
        assertEquals(detectLanguage.getLanguageCode(), "en");
    }

    @Test
    @DisplayName("listLanguage")
    public void getListLanguages() {
        YandexTranslateGateway yandexTranslateGateway = new YandexTranslateGateway();
        ListLanguages listLanguages = yandexTranslateGateway.getListLanguages("/languages");
        assertEquals(listLanguages.languages.size(), 88);
    }

    @SneakyThrows
    @Test
    @DisplayName("translate")
    public void getTranslate() {
        YandexTranslateGateway yandexTranslateGateway = new YandexTranslateGateway();
        ArrayList<String> text = new ArrayList<>();
        text.add("Hello World!");
        Translate translate = yandexTranslateGateway.getTranslate(text, "/translate");
        ArrayList<String> actual = new ArrayList<>();
        String str = "Всем Привет!";
        actual.add(str);
        assertEquals(translate.getTextTranslate(), actual);
    }
}
